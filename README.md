# ASP.NET MVCプログラム入門

ASP.NET MVCプログラム入門

## ASP.NETとMVCの概要

### ASP.NET

- ASP.NETとは
マイクロソフト社が提供しているWebアプリケーション開発の枠組み。

### MVC

- MVCとは
アプリケーションの構造を、下記の3つに分けること。
![1-1_ModelViewControllerの分離](images/1-1_ModelViewControllerの分離.drawio.png)
  - データとなる**Model**
  - 画面へ表示するための**View**
  - ユーザ入力を制御する**Controller**に分けること
- なぜMVCか
  - スキャフォールディング（足場）でCRUD画面およびコントローラーを自動で作成され、生産性が向上する
  - テスト自動化が可能となる
  Controllerの各メソッドをテストする。
  Modelのデータをチェックする。

### WebAPI

- WebAPIとは
WebAPIとは送受信データをViewではなくJSON形式やXML形式のデータにして渡す。

## スキャフォールディングの利用

スキャフォールディングとは、データベースにある既存のテーブルに対して検索と更新を行うためのViewとControllerを一気に作成すること。この機能によりマスターテーブルの編集機能を効率的に作ることができる。

![2-1_CRUD機能](images/2-1_CRUD機能.drawio.png)

### MVCパターンのルール

- Model：データベースのテーブルの1レコード
  - クラス名：データベースのテーブル名、単数形
  e.g. Book, Person, XxxEntity
  - プロパティ名：データベースのカラム名
- Controllerクラス
  - クラス名：Modelクラスの複数形 ＋ "Controller"
  e.g. BooksController, PeopleController, XxxEntitiesController
- View
  - Index.cshtml：一覧表示画面
  - Create.cshtml：新規作成画面
  - Detail.cshtml：詳細画面
  - Edit.cshtml：編集画面
  - Delete.cshtml：削除画面
- ブラウザのURL
  - e.g.
    - **Books**Controller → `http://localhost/Books/`
    BooksControllerのIndexメソッドが実行される
    - **Books**Controller → `http://localhost/Books/Edit/1`
    BooksControllerのEditメソッドが実行される
- Point
  - 設計時にMVCパターンを意識する必要がある
    - テーブル名など

### スキャフォールディングの利点と欠点

- 利点
  - 単一テーブルにアクセスするCRUD機能を一気に作れる
- 欠点
  - 複数テーブルで連携している場合には一工夫が必要
  ※ 外部参照があるときはには、自動でスキャフォールディングが追ってくれる

### まとめ

1. スキャフォールディングとは、**足場**という意味である。`ASP.NET MVC`のスキャフォールディングを使うと、足場のママでも使えるし、あとから機能を追加することも可能である。
2. `ASP.NET MVC`でプロジェクトを作成するとソリューションエクスプローラーに**Models**と**Controllers**と**Views**の3つのフォルダーが作成される。スキャフォールディングを実行すると、**Models**にあるEntityFrameworkのModelクラスを利用して、**Controllers**にはControllerクラスが、**Views**にはViewクラスが自動生成される。
3. `ASP.NET MVC`では、Modelクラスの単語の**複数形**がControllerくらすに付けられる。**複数形**の名称は、そのままViewを読み出すときのフォルダー名として使われる。デフォルトで**複数形**が推奨されているが、設定で変えることもできる。
4. `ASP.NET MVC`のViewクラスでは、拡張子が**cshmtl**のファイルが使われる。このファイルではRazor構文が使われHTML形式とC#のコードを混在させることができる。
5. CreateページやEditページで使われるformタグは、データを**POST**形式でサーバーをに送るために使われている。Controllerのメソッドには、**HttpPost**という属性を付けてガードを掛けている。

## Modelの活用

1. データベースからModelクラスを作成したり、逆にModelクラスからデータベースを作成するときには`dotnet ef`コマンドを使う。データベースからModelクラスを作るときは`dotnet ef dbcontext scaffold`を使い、逆にModelクラスからデータベースを作成するときには`dotnet ef database update`を使う。
2. Modelクラスを先に作成しておき、デバッグ実行でデータベースを作成する方法を`コードファースト`という。`コードファースト`はASP.NETMVCプロジェクトを作成して手早く実験するときに非常に有効である。
3. Modelクラスに属性として付加する情報を`アノテーション`という。`アノテーション`を使うと、Modelクラスの検証ロジックを統一的に扱うことができる。
4. 項目のタイトルを扱う属性は、`Display属性`を使う。通常はプロパティ名がEditページなどに表示されるが、`Display属性`を使うと項目の表示を日本語にできる。
5. 項目としては文字列の入力ではあるが、日付の入力や、Emailアドレス、Urlを区別するために`DataType属性`がある。`DataType属性`の列挙子を設定しておくと、入力時に日付のカレンダーを表示したり、EmailアドレスやUrlのフォーマットをチェックしてくれる。

## Viewの活用

1. ASP.NET アプリケーションのViewは、`Razor構文`と呼ばれるC#とHTMLタグが混在できる構文を使って作成する。Viewno拡張子は`cshtml`になる
2. ViewページにマッピングされたModelクラスは、そのまま「`@Model`.プロパティ名」のように使えるが、このままでインテリセンスが効かない。Viewページの先頭に`@model`Modelクラスの型の行を入れると、型がチェックされるためインテリセンスが有効になり、プロパティ名の記述などのミスが減る
3. Viewページでは、従来のHTMLヘルパーから`タグヘルパー`を使うように変更されている`タグヘルパー`では、既存のHTMLタグをasp-for属性やasp-action属性などの拡張属性を使って、実行時にHTMLタグに変換する
4. Viewページでは、Modelクラスに含まれないデータをViewDataや`ViewBag`を使ってやり取りできる。設定する型は`object`に変換されてしまうため、利用するときにキャストが必要な場合がある
5. Viewページでは共通のレイアウトが利用されている。共通レイアウトのファイル内で各Viewページが表示される設定になる。このためメニューなどを共通化できる。共通化レイアウトのファイルの位置は「Views/`Shared`/_Layout.cshtml」になる。

## Controllerの活用

1. ControllerクラスのActionメソッドは、非同期型と同期型がある。Actionメソッドが戻すViewの型は`IActionResult`となるため、非同期処理の場合には、「async Task `IActionResult` Index()」のように書く。同期処理の場合は「`IActionResult` Index()」になる。
2. Actionメソッドの引数は、URLアドレスに埋め込まれた通常のパラメーターと、フォーム入力で`POSTメソッド`形式で送信されたデータをマッピングするパラメーターがある。`POSTメソッド`でのマッピングは、引数に`Bind`属性を付けて行う。
3. Actionメソッドを実行した後に、別のControllerクラスのActionメソッドを呼び出したいときは、`RedirectToAction`メソッドを使う。Controller名とActionメソッド名を指定できる。特定のURLにジャンプさせたいときは、`Redirect`メソッドを使うと良い。
4. データベースの処理を行うメソッドはフォーム入力で使う`POSTメソッド`形式を使う。このとき、Actionメソッドには`HttpPost`属性を付けてそれ以外の方法で呼び出されたときのガードを掛ける。また、ログインしている状態だけ有効にするAuthorize属性もある。

## List-Detailの関係

1. `コードファースト`を使ってModelクラスからデータベースのテーブルを作成するときに、テーブル同士のリレーションを設定できる。書籍テーブルの`Book`から著者テーブルの`Author`を参照するときは次のように参照先のテーブルをプロパティで指定する。
`public virtual Author Author {get; set;}`
逆に著者テーブルの`Author`から書籍テーブルの`Book`を参照させて、執筆した本を取得できるようにするために、コレクションを設定しておく。
`public virtual ICollection<Book> Book {get; set;}`
2. リンク先のプロパティやコレクションは、LINQで検索するときに`Include`メソッドを使って、リレーション先のデータも取得できるようにしておく。

```csharp
var book = await _context.Book
    .Include(b => b.Author)
    .Include(b => b.Publisher)
    .SingleOrDefaultAsync(m => m.Id == id);
```

3. ページング機能を実装するときに、タグヘルパーを使うとリンクが作りやすい。次の例では、呼び出すActionメソッドとして`Include`属性と、Indexメソッドの`page`引数に合わせてasp-route-`page`属性を指定する。

```csharp
<a asp-action="Index" asp-route-page="@ViewBag.Prev">prev</a>

public async Task<IActionResult> Index(int? page)
```
