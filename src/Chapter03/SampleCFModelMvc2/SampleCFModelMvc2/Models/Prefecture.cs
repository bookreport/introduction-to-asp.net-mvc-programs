﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace SampleCFModelMvc2.Models
{
    public class Prefecture
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static void Initialize(DbContext context)
        {
            var t = context.Set<Prefecture>();
            if (t.Any() == false)
            {
                t.AddRange(
                    new Prefecture { Name = "北海道" },
                    new Prefecture { Name = "青森県" },
                    new Prefecture { Name = "岩手県" },
                    new Prefecture { Name = "宮城県" },
                    new Prefecture { Name = "秋田県" },
                    new Prefecture { Name = "山形県" });
                context.SaveChanges();
            }
        }
    }
}