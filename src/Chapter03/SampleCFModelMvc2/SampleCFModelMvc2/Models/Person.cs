﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SampleCFModelMvc2.Models
{
    public class Person
    {
        public int Id { get; set; }

        [DisplayName("名前")]
        [Required]
        [MaxLength(20, ErrorMessage = "最大文字数は20文字までです")]
        public string Name { get; set; }

        [DisplayName("年齢")]
        [Range(18, 100, ErrorMessage = "年齢は18歳から100歳までです")]
        [DisplayFormat(DataFormatString = "{0} 歳")]
        public int Age { get; set; }

        [DisplayName("出身地")]
        public int PrefectureId { get; set; }

        [DisplayName("出身地")]
        public Prefecture Prefecture { get; set; }

        [DisplayName("入社日")]
        [DisplayFormat(DataFormatString = "{0:yyyy年MM月dd日}")]
        [DataType(DataType.Date)]
        [ValidationHireDate]
        public DateTime? HireDate { get; set; }

        [DisplayName("出勤状態")]
        public bool IsAttendance { get; set; }

        [DisplayName("メールアドレス")]
        [EmailAddress(ErrorMessage = "メールアドレスを入力してください")]
        public string Email { get; set; }

        [DisplayName("ブログURL")]
        [Url]
        public string Blog { get; set; }

        [DisplayName("社員番号")]
        [RegularExpression("[A-Z]{3}-[0-9]{4}")]
        public string EmployeeNo { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class ValidationHireDate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime date = Convert.ToDateTime(value);
                if (date > DateTime.Now)
                {
                    return new ValidationResult("入社日は本日以前を指定してください");
                }
            }
            return ValidationResult.Success;
        }
    }
}