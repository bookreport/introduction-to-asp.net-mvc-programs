﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SampleDBModelMvc.Models
{
    public partial class Prefecture
    {
        public Prefecture()
        {
            People = new HashSet<Person>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Person> People { get; set; }
    }
}
