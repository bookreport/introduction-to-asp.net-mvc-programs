﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SampleCFModelMvc.Data.Migrations
{
    public partial class ChangePerson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PrefectureId",
                table: "Person",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Prefecture",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prefecture", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Person_PrefectureId",
                table: "Person",
                column: "PrefectureId");

            migrationBuilder.AddForeignKey(
                name: "FK_Person_Prefecture_PrefectureId",
                table: "Person",
                column: "PrefectureId",
                principalTable: "Prefecture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Person_Prefecture_PrefectureId",
                table: "Person");

            migrationBuilder.DropTable(
                name: "Prefecture");

            migrationBuilder.DropIndex(
                name: "IX_Person_PrefectureId",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "PrefectureId",
                table: "Person");
        }
    }
}
