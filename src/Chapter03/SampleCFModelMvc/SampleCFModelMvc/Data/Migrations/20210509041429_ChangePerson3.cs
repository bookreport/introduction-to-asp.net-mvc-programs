﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SampleCFModelMvc.Data.Migrations
{
    public partial class ChangePerson3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Person_Prefecture_PrefectureId",
                table: "Person");

            migrationBuilder.AlterColumn<int>(
                name: "PrefectureId",
                table: "Person",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Person_Prefecture_PrefectureId",
                table: "Person",
                column: "PrefectureId",
                principalTable: "Prefecture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Person_Prefecture_PrefectureId",
                table: "Person");

            migrationBuilder.AlterColumn<int>(
                name: "PrefectureId",
                table: "Person",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Person_Prefecture_PrefectureId",
                table: "Person",
                column: "PrefectureId",
                principalTable: "Prefecture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
