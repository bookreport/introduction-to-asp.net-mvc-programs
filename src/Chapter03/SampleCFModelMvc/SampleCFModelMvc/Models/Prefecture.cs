﻿namespace SampleCFModelMvc.Models
{
    public class Prefecture
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}