﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace SampleListDetailMvc.Data
{
    /// <summary>
    /// アプリケーションDBコンテキスト
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="options">DBコンテキストオプション</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// <summary>書籍データセット</summary>
        public DbSet<SampleListDetailMvc.Models.Book> Book { get; set; }

        /// <summary>著者データセット</summary>
        public DbSet<SampleListDetailMvc.Models.Author> Author { get; set; }

        /// <summary>出版社データセット</summary>
        public DbSet<SampleListDetailMvc.Models.Publisher> Publisher { get; set; }
    }
}