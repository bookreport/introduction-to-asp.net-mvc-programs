﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.ComponentModel;

namespace SampleListDetailMvc.Models
{
    /// <summary>
    /// 著者
    /// </summary>
    public class Author
    {
        /// <summary>ID</summary>
        public int Id { get; set; }

        /// <summary>著者名</summary>
        [DisplayName("著者名")]
        public string Name { get; set; }

        /// <summary>年齢</summary>
        [DisplayName("年齢")]
        public int Age { get; set; }

        /// <summary>都道府県IDに</summary>
        public int PrefectureId { get; set; }

        /// <summary>書籍</summary>
        public virtual ICollection<Book> Books { get; set; }

        /// <summary>出身地</summary>
        [DisplayName("出身地")]
        public virtual Prefecture Prefecture { get; set; }
    }
}