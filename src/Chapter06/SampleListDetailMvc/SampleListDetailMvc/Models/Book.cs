﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SampleListDetailMvc.Models
{
    /// <summary>
    /// 書籍
    /// </summary>
    public class Book
    {
        /// <summary>Id</summary>
        public int Id { get; set; }

        /// <summary>書名</summary>
        [DisplayName("書名")]
        public string Title { get; set; }

        /// <summary>著者ID</summary>
        [DisplayName("著者")]
        public int AuthorId { get; set; }

        /// <summary>出版社ID</summary>
        [DisplayName("出版社")]
        public int PublisherId { get; set; }

        /// <summary>価格</summary>
        [DisplayName("価格")]
        public int Price { get; set; }

        /// <summary>著者</summary>
        [DisplayName("著者")]
        public virtual Author Author { get; set; }

        /// <summary>出版社</summary>
        [DisplayName("出版社")]
        public virtual Publisher Publisher { get; set; }

        /// <summary>発売日</summary>
        [DisplayName("発売日")]
        [DataType(DataType.Date)]
        public DateTime? PublishDate { get; set; }

        /// <summary>ISBNコード</summary>
        [DisplayName("ISBNコード")]
        [RegularExpression("[0-9]{3}-[0-9]{1}-[0-9]{3,5}-[0-9]{3,5}-[0-9A-Z]{1}")]
        public string ISBN { get; set; }
    }
}