﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace SampleListDetailMvc.Models
{
    /// <summary>
    /// エラービューモデル
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>リクエストID</summary>
        public string RequestId { get; set; }

        /// <summary>リクエストIDを表示する</summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
