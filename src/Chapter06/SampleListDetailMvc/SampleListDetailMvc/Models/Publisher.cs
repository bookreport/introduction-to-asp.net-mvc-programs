﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.ComponentModel;

namespace SampleListDetailMvc.Models
{
    /// <summary>
    /// 出版社
    /// </summary>
    public class Publisher
    {
        /// <summary>ID</summary>
        public int Id { get; set; }

        /// <summary>出版社名</summary>
        [DisplayName("出版社名")]
        public string Name { get; set; }

        /// <summary>書籍</summary>
        public virtual ICollection<Book> Books { get; set; }
    }
}
