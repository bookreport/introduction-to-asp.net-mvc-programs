﻿using Microsoft.AspNetCore.Mvc;

namespace SampleHelloMvc.Controllers
{
    public class HelloController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Message"] = "Let's study ASP.NET MVC";

            return View();
        }
    }
}